#pragma once

#include <maya/MPxCommand.h>

#include <maya/MSyntax.h>
#include <maya/MStatus.h>
#include <maya/MArgList.h>
#include <maya/MSelectionList.h>


class BakeCommand : public MPxCommand
{
public:
	BakeCommand();
	virtual 		~BakeCommand();
	static	void*	creator();
	static	MSyntax	newSyntax();
	virtual MStatus	doIt(const MArgList& argList);


private:
	// flags
	bool raySourceFlagSet, rayDirectionFlagSet;

	MSelectionList sList;
};
