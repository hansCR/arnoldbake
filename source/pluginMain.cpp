/*
 * pluginMain.cpp
 *
 *      Author: hans.cr
 */

#include <maya/MFnPlugin.h>
#include <ai.h>

#include "arnoldVertexBake.h"
#include "constants.h"

MStatus initializePlugin( MObject obj )
{
    MStatus status;

    MFnPlugin fnPlugin( obj, AUTHOR, PLUGIN_VERSION, "Any" );

    status = fnPlugin.registerCommand( "bakeArnoldVertexColor", BakeCommand::creator, BakeCommand::newSyntax );
    CHECK_MSTATUS_AND_RETURN_IT( status );
    
    return MS::kSuccess;
}


MStatus uninitializePlugin( MObject obj )
{
    MStatus status;

    MFnPlugin fnPlugin( obj );
    status = fnPlugin.deregisterCommand( "bakeArnoldVertexColor" );
    CHECK_MSTATUS_AND_RETURN_IT( status );

    return MS::kSuccess;
}
