///////////////////////////////////////////////////////////////////////////////
//
// arnoldVertexBake MEL command
//
// Bakes vertex colors using arnold.
//
////////////////////////////////////////////////////////////////////////////////


#include "arnoldVertexBake.h"

#include <maya/MSyntax.h>
#include <maya/MArgDatabase.h>
#include <maya/MDagPath.h>
#include <maya/MItMeshFaceVertex.h>
#include <maya/MFnMesh.h>
#include <maya/MIntArray.h>

#include <ai.h>

bool TraceRay(
		const MPoint &inPoint,
		const MVector &inNormal,
		const double &offset,
		AtShaderGlobals* sg,
		AtScrSample& result)
{
	AtRay ray;
	AtVector origin, direction;
	origin.x = float(inPoint.x + inNormal.x * offset);
	origin.y = float(inPoint.y + inNormal.y * offset);
	origin.z = float(inPoint.z + inNormal.z * offset);

	direction.x = float(-inNormal.x);
	direction.y = float(-inNormal.y);
	direction.z = float(-inNormal.z);

	ray = AiMakeRay(AI_RAY_ALL_DIFFUSE, origin, &direction, (float)offset*10, sg);
	return AiTrace(ray, AtRGB(1, 1, 1), result);
}

BakeCommand::BakeCommand()
{
}

BakeCommand::~BakeCommand()
{
}

void * BakeCommand::creator()
{
	return new BakeCommand;
}

MSyntax BakeCommand::newSyntax()
{
	MSyntax syntax;

	syntax.addFlag("-o", "-offset", MSyntax::kDouble);

	syntax.useSelectionAsDefault(true);
	syntax.setObjectType(MSyntax::kSelectionList, 1);

	syntax.enableEdit(false);
	syntax.enableQuery(false);

	return syntax;
}

MStatus BakeCommand::doIt(const MArgList & argList)
{
	MStatus status;


	// Read all the flag arguments
	MArgDatabase argData(syntax(), argList, &status);
	CHECK_MSTATUS_AND_RETURN_IT(status);

	// STORE FLAG INDICATORS, STORING WHETHER EACH FLAG HAS BEEN SET OR NOT:
	bool offseteFlagSet = argData.isFlagSet("-offset");

	double offset = 0.1;
	if (offseteFlagSet)
		argData.getFlagArgument("-offset", 0, offset);

	status = argData.getObjects(sList);
	CHECK_MSTATUS_AND_RETURN_IT(status);

	MDagPath bakeMesh;
	status = sList.getDagPath(0, bakeMesh);
	CHECK_MSTATUS_AND_RETURN_IT(status);

	AiRender(AI_RENDER_MODE_FREE);

	AtShaderGlobals * sg = AiShaderGlobals();
	AtScrSample result;
	MPoint position;
	MVector normal;

	MColorArray colors;
	MIntArray verts, faces;
	bool hit;

	MFnMesh fnmesh(bakeMesh);
	MItMeshFaceVertex it(bakeMesh);
	for (; !it.isDone(); it.next())
	{
		position = it.position(MSpace::kWorld, &status);
		status = it.getNormal(normal, MSpace::kWorld);
		verts.append(it.vertId());
		faces.append(it.faceId());

		hit = TraceRay(position, normal, offset, sg, result);
		if (not hit)
		{
			fnmesh.getVertexNormal(it.vertId(), true, normal, MSpace::kWorld);
			hit = TraceRay(position, normal, offset, sg, result);
			if(not hit)
			{
				result.color.r = 1;
				result.color.g = 0;
				result.color.b = 0;
				result.alpha = 1;
			}
		}
		AtRGB outcolor = result.color;
		colors.append(MColor(outcolor.r, outcolor.g, outcolor.b, result.alpha));
	}

	fnmesh.setFaceVertexColors(colors, faces, verts, NULL, MFnMesh::kRGBA);
	fnmesh.updateSurface();

	return MStatus::kSuccess;
}
