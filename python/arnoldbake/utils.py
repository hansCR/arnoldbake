import maya.cmds as cmds
import maya.api.OpenMaya as om
import arnold as ai


def create_colorset(node, name='previewColors'):
    """
    Creates a colorSet with the given name to the given node.
    """

    if name not in (cmds.polyColorSet(node, q=True, allColorSets=True) or []):
        cmds.polyColorSet(node, create=True, colorSet=name)
        om.MGlobal.displayInfo('Added colorSet "{}" to {}'.format(name, node))
    cmds.polyColorSet(node, currentColorSet=True, colorSet=name)
    cmds.setAttr(node + '.displayColors', True)


def extend_to_meshes(nodes, longpaths=False):
    meshes = cmds.ls(nodes, type='mesh', long=longpaths) or []
    if longpaths:
        meshes.extend(cmds.listRelatives(nodes, allDescendents=True,
                                         type='mesh', fullPath=True) or [])
    else:
        meshes.extend(cmds.listRelatives(nodes, allDescendents=True,
                                         type='mesh', path=True) or [])
    return meshes

def bake_selected():
    cmds.loadPlugin('mtoa', quiet=True)
    cmds.loadPlugin('arnoldbake', quiet=True)

    cmds.arnoldScene(mode='convert_scene')
    
    node_mapping = {}
    iter = ai.AiUniverseGetNodeIterator(ai.AI_NODE_SHAPE);
    while not ai.AiNodeIteratorFinished(iter):
        arnold_node = ai.AiNodeIteratorGetNext(iter)
        arnold_node_name = ai.AiNodeGetName(arnold_node)
        if arnold_node_name == 'root':
            continue
        node_mapping[arnold_node_name] = arnold_node
        ai.AiNodeSetByte(arnold_node, 'visibility', 0)
    ai.AiNodeIteratorDestroy(iter);

    longpaths = cmds.getAttr("defaultArnoldRenderOptions.exportFullPaths")
    selection = extend_to_meshes(cmds.ls(sl=True), longpaths)
    
    for each in selection:
        arnold_node = node_mapping.get(each)
        if not arnold_node:
            print 'node not found: "{}"'.format(each)
            continue
        ai.AiNodeSetByte(arnold_node, 'visibility', 255)
        create_colorset(each)
        cmds.bakeArnoldVertexColor(each, offset=2)
        ai.AiNodeSetByte(arnold_node, 'visibility', 0)

    cmds.arnoldScene(mode='destroy')
    cmds.ogs(reset=True)
